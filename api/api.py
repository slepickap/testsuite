#module that interacts with the gitlab API
import requests
import os
BASE_URL = "https://gitlab.com/api/v3"
def Login():
    """Logs user into GITLAB"""
    #response = requests.post('https://gitlab.com/api/v3/session?login=slepickap&password=godgod11')
    response = requests.post( BASE_URL + '/session/?login=' + os.environ['GITLAB_USER']+ '&password=' + os.environ['GITLAB_PW'])
    return response
def project_list():
    """List all projects for the logged in user"""
    headers= {'PRIVATE-TOKEN': Login().json()['private_token']}
    print headers
    return requests.get ('https://gitlab.com/api/v3/projects', headers=headers)
  