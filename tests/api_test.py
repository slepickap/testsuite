import sys
sys.path.insert(0,'api')
import api
def test_login():
    response = api.Login()
    assert response.status_code == 201
    assert 'pavel-slepicka@seznam.cz' in str ( response.json())
def test_project():
    response=api.project_list()
    assert response.status_code == 200
    assert "testsuite-jobs" in str(response.json())