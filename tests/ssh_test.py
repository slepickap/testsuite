import os
import sys
sys.path.insert(0,'ssh')
import ssh
def test_create_connection():
  result = ssh.create_connection()
  print result
  stdin, stdout , stderr = result.exec_command('echo "$USER"')
  assert stdout.read().strip('\n') == os.environ['SSH_USER']
def test_upload_file():
  target_path = "/home/commandemy/log.txt"
  result = ssh.upload_file('log.txt', target_path) == True
  assert result == True
def test_file_exist():
  target_path = "/home/commandemy/log.txt"
  result = ssh.file_exist(target_path)
  assert  result
  
def test_parse_log():
    target_path = "/home/commandemy/log.txt"
    assert "EULA has been accepted" in ssh.file_read(target_path)
    assert "Filesystem is mounted" in ssh.file_read(target_path)
    assert "Finished. Syncing..." in ssh.file_read(target_path)
